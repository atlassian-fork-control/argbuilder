# NOTE: This script comes from https://bitbucket.org/atlassian/argbuilder commit <HEAD>
from argparse import ArgumentParser
import json
import logging
import os
import re
import sys
import yaml

logger = None
ARGBUILDER_CONFIG = '%s/argbuilder-config.yaml'
ENV_VAR_PATTERN = 'env:\/\/\/(\w+)'
SOURCE_FILE_TOKEN_PATTERN = '%{::(.*?)}'


class ArgumentBuilder(object):
    # config_type: sysprops, java_opts, or both
    # hierarchy config found at <config_dir>/<platform>.hierarchy
    # env_map config found at <config_dir>/platform.envmap.yaml

    def __init__(self, builder_type, config_dir, platform):
        self.builder_type = builder_type
        self.platform = platform

        if config_dir is None:
            self.config_dir = '%s/runconf' % os.path.dirname(os.path.realpath(__file__))

            if not os.path.exists(self.config_dir):
                raise RuntimeError(("CATALINA_DOCBASE environment variable doesn't exist, please "
                                    "use --confdir to specify config directory"))
        else:
            self.config_dir = config_dir

        self.spr = re.compile(SOURCE_FILE_TOKEN_PATTERN)
        self.evp = re.compile(ENV_VAR_PATTERN)

        self.config = self.get_config()
        logger.debug('config: %s' % json.dumps(self.config))

        self.env_map = self.get_platform_env_map()
        logger.debug('env_map: %s' % json.dumps(self.env_map))

        self.hierarchy = self.get_hierarchy()
        logger.debug('hierarchy: %s' % json.dumps(self.hierarchy))

        self.source_files = self.get_source_files()
        logger.debug('source_files: %s' % json.dumps(self.source_files))

    def get_config(self):
        with open(ARGBUILDER_CONFIG % self.config_dir, 'r') as data:
            doc = yaml.load(data)
        return doc

    def get_platform_env_map(self):
        if 'env_map' in self.config and self.platform in self.config['env_map']:
            return self.config['env_map'][self.platform]
        else:
            # raise RuntimeError('No environment mappings for platform %s' % self.platform)
            logger.info('No environment mappings for platform %s' % self.platform)
            return None

    def get_hierarchy(self):
        hierarchy = []

        if 'hierarchy' in self.config and self.platform in self.config['hierarchy']:
            for line in self.config['hierarchy'][self.platform]:
                hierarchy.append(line.strip())

        hierarchy.reverse()

        return hierarchy

    def get_filepattern_value(self, key):
        # %{::platform} is a special cased to separate config structures to
        # avoid confusion due to the different usage of eg. zone etc
        if key == 'platform':
            return self.platform
        else:
            if key in self.env_map:
                if self.env_map[key] in os.environ:
                    # NOTE: Special handling for eg. rack-101 -> 101
                    if 'rack-' in os.environ[self.env_map[key]]:
                        return re.sub(r'rack-', '', os.environ[self.env_map[key]])
                    else:
                        return os.environ[self.env_map[key]]
                else:
                    logger.error('%s not found in the environment' % self.env_map[key])
                    return None
            else:
                logger.error('Unknown substitution key: %%{::%s}' % key)
                return None

    def get_source_files(self):
        config_files = []

        # Iterate through list of possible properties files
        for file_pattern in self.hierarchy:
            skip_source = False

            # Test file pattern for substitutions then iterate through and
            # replace as appropriate
            source_file = file_pattern
            source_keys = self.spr.findall(file_pattern)
            if source_keys:
                # If a file pattern contains an unknown substitution, print a
                # message to stderr, then skip the offending file pattern
                for key in source_keys:
                    value = self.get_filepattern_value(key)
                    if value is None:
                        skip_source = True
                        continue
                    else:
                        source_file = re.sub(r'%' + re.escape('{::%s}' % key), value, source_file)

            if not skip_source:
                source_file = source_file + '.properties'
                config_files.append(source_file)

        return config_files

    def merge_config(self, config_type, merge_by_reset=False):
        arguments = {}
        for source_file in self.source_files:
            fn = '%s/%s/%s' % (self.config_dir, config_type, source_file)

            if os.path.isfile(fn):
                if os.stat(fn).st_size == 0:
                    logger.debug('File %s is empty' % fn)
                    continue

                # TODO: add a try/catch to handle if we are unable to read file
                with open(fn, 'r') as data:
                    if merge_by_reset:
                        arguments = {}

                    for line in data:
                        line = line.strip()

                        # Is line empty, or a comment?
                        if not line or is_comment(line):
                            continue

                        # Assume sysprops are <key>=<value>
                        # Assume <key> doesn't contain an '='
                        # Split on first '=' (in case value has multiple '=')
                        line_arr = line.split('=', 1)
                        if len(line_arr) == 2:
                            key = line_arr[0]
                            value = line_arr[1]

                            # Does it contain env:///
                            match = self.evp.search(value)
                            if match:
                                # Match 0 is the entire string, 1 is the capture
                                env_key = match.group(1)
                                if env_key in os.environ:
                                    value = os.environ[env_key]
                                else:
                                    # No corresponding value in the environment
                                    logger.error('%s was not found in the environment' % env_key)
                                    continue

                            arguments[key] = value
                        else:
                            if config_type == 'java_opts':
                                arguments[line] = None
                            else:
                                logger.info('Unknown format for sysprop: %s' % line)
            else:
                # logger.info("File %s doesn't exist" % fn)
                pass

        return arguments

    def build_arguments(self):
        java_opt_hash = {}
        java_opts = ''
        sysprop_hash = {}
        sysprops = ''

        # TODO: switch this to have class props eg self.build_java_opts + self.build_sysprops?
        if self.builder_type in ['java_opts', 'both']:
            java_opt_hash = self.merge_config('java_opts', merge_by_reset=True)
            java_opts = concat_java_opts(java_opt_hash)

        if self.builder_type in ['sysprops', 'both']:
            sysprop_hash = self.merge_config('sysprops')
            sysprops = concat_sysprops(sysprop_hash)

        if self.builder_type in ['java_opts', 'both']:
            logger.debug('java_opt hash: %s' % json.dumps(java_opt_hash))
        if self.builder_type in ['sysprops', 'both']:
            logger.debug('sysprop hash: %s' % json.dumps(sysprop_hash))

        arguments = java_opts + ' ' + sysprops

        return arguments


def concat_java_opts(arg_hash):
    arguments = []

    for k, v in list(arg_hash.items()):
        if v is None or re.match(r'XX:[+|-]{1}.+', k):
            arguments.append('-%s' % k)
        else:
            if re.match(r'X(ms|mx|ss)', k):
                # Should this be /X(ms|mx|ss)$/?
                arguments.append('-%s%s' % (k, v))
            elif re.match(r'X[X:]?.+', k):
                arguments.append('-%s=%s' % (k, v))
            else:
                arguments.append('-%s:%s' % (k, v))

    return ' '.join(sorted(arguments))


def concat_sysprops(arg_hash):
    arguments = []

    for k, v in list(arg_hash.items()):
        arguments.append('-D%s=%s' % (k, v))

    return ' '.join(sorted(arguments))


def is_unicorn():
    env_keys = list(os.environ.keys())
    return 'UNICORN_APPLICATION_ENVIRONMENT' in env_keys or \
           'UNICORN_AREA' in env_keys or \
           'UNICORN_ZONE' in env_keys


def is_vertigo():
    env_keys = list(os.environ.keys())
    return 'MICROS_ENV' in env_keys or \
           'MICROS_AWS_REGION' in env_keys or \
           'VERTIGO_ZONE' in env_keys


def is_comment(line):
    if re.match(r'^\s*#', line):
        return True
    else:
        return False


def get_logger():
    log_format = '%(asctime)s %(levelname)s [%(name)s] %(message)s'
    log_level = logging.INFO

    logging.basicConfig(
        format=log_format,
        level=log_level,
        stream=sys.stderr
    )
    return logging.getLogger('argbuilder')


def get_argument_parser():
    parser = ArgumentParser(
        description=(
            'Parses tree of properties files and prints out an argument string'
            ' for running confluence.  NOTE: Sysprops files will merge, '
            'java_opts files will reset and override'
        )
    )
    parser.add_argument(
        '--type',
        choices=['sysprops', 'java_opts', 'both'],
        default='both',
        help='Type of arguments to build'
    )
    parser.add_argument(
        '--confdir',
        help='Location of config to turn into arguments'
    )
    parser.add_argument(
        '--platform',
        choices=['unicorn', 'vertigo', 'dev'],
        required=True,
        help='Type of arguments to build'
    )
    parser.add_argument(
        '--debug',
        action='store_true',
        default=False,
        help='Print debug info to stderr'
    )

    return parser


def main():
    global logger
    logger = get_logger()
    parser = get_argument_parser()

    arguments = parser.parse_args()
    builder_type = arguments.type
    platform = arguments.platform

    if arguments.confdir:
        config_dir = arguments.confdir
    else:
        config_dir = None

    if arguments.debug:
        logger.setLevel(logging.DEBUG)

    ab = ArgumentBuilder(builder_type, config_dir, platform)
    print(ab.build_arguments())

if __name__ == '__main__':
    main()
