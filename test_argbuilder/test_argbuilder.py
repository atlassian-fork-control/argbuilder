import argbuilder
import logging
import os
import sys
import unittest2 as unittest

log_format = '%(asctime)s %(levelname)s [%(name)s] %(message)s'
log_level = logging.ERROR

logging.basicConfig(
    format=log_format,
    level=log_level,
    stream=sys.stderr
)


class TestArgbuilderMethods(unittest.TestCase):
    def setUp(self):
        os.environ['UNICORN_APPLICATION_ENVIRONMENT'] = 'jirastudio-prd'
        os.environ['UNICORN_AREA'] = 'rack-106'
        os.environ['UNICORN_ZONE'] = 'sc1'
        os.environ['VALUE_ONE'] = '1'
        os.environ['VALUE_TWO'] = '2'
        os.environ['VALUE_THREE'] = '3'
        os.environ['VALUE_FOUR'] = '4'
        os.environ['VALUE_FIVE'] = '5'

        argument_type = 'both'
        runconf = '%s/fixtures/runconf' % os.path.dirname(os.path.realpath(__file__))
        platform = 'unicorn'
        argbuilder.logger = logging.getLogger('argbuilder-test')

        self.maxDiff = None
        self.ab = argbuilder.ArgumentBuilder(argument_type, runconf, platform)

    def test_get_config(self):
        pass

    def test_get_platform_env_map(self):
        expected = {
            'environment': 'UNICORN_APPLICATION_ENVIRONMENT',
            'rack': 'UNICORN_AREA',
            'zone': 'UNICORN_ZONE'
        }

        self.assertEqual(self.ab.get_platform_env_map(), expected)

    def test_get_hierarchy(self):
        expected = [
            'global',
            '%{::platform}/common',
            '%{::platform}/environments/%{::environment}',
            '%{::platform}/zones/%{::zone}',
            '%{::platform}/racks/%{::rack}',
            '%{::platform}/zones/%{::zone}/environments/%{::environment}',
            '%{::platform}/zones/%{::zone}/environments/%{::environment}/racks/%{::rack}',
            'local',
        ]

        self.assertEqual(self.ab.get_hierarchy(), expected)

    def test_get_filepattern_value(self):
        expected_platform = 'unicorn'
        expected_environment = 'jirastudio-prd'
        expected_zone = 'sc1'
        expected_rack = '106'

        self.assertEqual(self.ab.get_filepattern_value('platform'), expected_platform)
        self.assertEqual(self.ab.get_filepattern_value('environment'), expected_environment)
        self.assertEqual(self.ab.get_filepattern_value('zone'), expected_zone)
        self.assertEqual(self.ab.get_filepattern_value('rack'), expected_rack)

    def test_get_filepattern_value_missing_keys(self):
        # Setup a new argbuilder without putting VERTIGO_ZONE in the environment
        os.environ['MICROS_ENV'] = 'ddev'
        os.environ['MICROS_AWS_REGION'] = 'ap-southeast-2'
        runconf = '%s/fixtures/runconf' % os.path.dirname(os.path.realpath(__file__))
        platform = 'vertigo'
        argbuilder.logger = logging.getLogger('argbuilder-test')
        test_ab = argbuilder.ArgumentBuilder('both', runconf, platform)

        expected_platform = platform
        expected_environment = 'ddev'
        expected_region = 'ap-southeast-2'

        self.assertEqual(test_ab.get_filepattern_value('platform'), expected_platform)
        self.assertEqual(test_ab.get_filepattern_value('environment'), expected_environment)
        self.assertEqual(test_ab.get_filepattern_value('region'), expected_region)

    def test_get_source_files(self):
        expected = [
            'global.properties',
            'unicorn/common.properties',
            'unicorn/environments/jirastudio-prd.properties',
            'unicorn/zones/sc1.properties',
            'unicorn/racks/106.properties',
            'unicorn/zones/sc1/environments/jirastudio-prd.properties',
            'unicorn/zones/sc1/environments/jirastudio-prd/racks/106.properties',
            'local.properties',
        ]

        self.assertEqual(self.ab.get_source_files(), expected)

    def test_merge_config(self):
        expected = {
            'feature.a': '1',
            'feature.b': '2',
            'feature.c': '3',
            'feature.d': '4',
            'global': 'true',
            'global.prop1': 'default',
            'global.prop2': 'unicorn.common.override',
            'global.prop3': 'unicorn.environments.jirastudio-prd.override',
            'global.prop4': 'unicorn.zones.sc1.override',
            'global.prop5': 'unicorn.racks.106.override',
            'global.prop6': 'unicorn.zones.sc1.environments.jirastudio-prd.override',
            'global.prop7': 'unicorn.zones.sc1.environments.jirastudio-prd.racks.106.override',
            'unicorn.common': 'true',
            'unicorn.environments.jirastudio-prd': 'true',
            'unicorn.racks.106': 'true',
            'unicorn.zones.sc1': 'true',
            'unicorn.zones.sc1.environments.jirastudio-prd': 'true',
            'unicorn.zones.sc1.environments.jirastudio-prd.racks.106': 'true',
        }

        self.assertEqual(self.ab.merge_config('sysprops', False), expected)

    def test_build_arguments(self):
        expected = ' -Dfeature.a=1 -Dfeature.b=2 -Dfeature.c=3 -Dfeature.d=4 -Dglobal.prop1=default -Dglobal.prop2=unicorn.common.override -Dglobal.prop3=unicorn.environments.jirastudio-prd.override -Dglobal.prop4=unicorn.zones.sc1.override -Dglobal.prop5=unicorn.racks.106.override -Dglobal.prop6=unicorn.zones.sc1.environments.jirastudio-prd.override -Dglobal.prop7=unicorn.zones.sc1.environments.jirastudio-prd.racks.106.override -Dglobal=true -Dunicorn.common=true -Dunicorn.environments.jirastudio-prd=true -Dunicorn.racks.106=true -Dunicorn.zones.sc1.environments.jirastudio-prd.racks.106=true -Dunicorn.zones.sc1.environments.jirastudio-prd=true -Dunicorn.zones.sc1=true'

        self.assertEqual(self.ab.build_arguments(), expected)

    @unittest.skip("test_concat_java_opts: not implemented")
    def test_concat_java_opts(self):
        pass

    def test_concat_sysprops(self):
        data = {'prop1': 'val1', 'prop2': 'val2'}
        expected = '-Dprop1=val1 -Dprop2=val2'

        self.assertEqual(argbuilder.concat_sysprops(data), expected)

    @unittest.skip("test_is_unicorn: not implemented")
    def test_is_unicorn(self):
        pass

    @unittest.skip("test_is_vertigo: not implemented")
    def test_is_vertigo(self):
        pass

    def test_iscomment(self):
        self.assertTrue(argbuilder.is_comment('# A comment'))
        self.assertFalse(argbuilder.is_comment('Not a comment'))


if __name__ == '__main__':
    import xmlrunner
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'))
